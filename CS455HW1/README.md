# CS455 HW1

Implementation of assignment CS455 HW1 for Fall 2017. 

In order to build the project and generate the necessary class files, run the following command from the /CS455HW1/src path:

make all


In order to clean up previously generated class files, please run "make clean" from the same path.



# RUNNING THE SOFTWARE

You can run either spawn a Registry node or a MessagingNode using the software. These two types of nodes constitute the two main starting points for execution of this project.

There can be one Registry and multiple MessagingNode instances.


# Starting Registry node instance

Run the following command from the terminal while you are at the path /CS455HW1/src :

java cs455.overlay.node.Registry <portnum>
Here <portnum> is a free port number that you enter as argument to this program.


# Starting MessagingNode instance

Run the following command from the terminal while you are at the path /CS455HW1/src :
java cs455.overlay.node.MessagingNode <registry-host> <registry-port>
Here, <registry-host> is the hostname of the node where you started the Registry node
<registry-port> is the port number we gave as argument while spawning the Registry node


# NOTE:

This software implements all the functionality required in the Assignment. 
This software allows registration and deregistration of nodes. Although, it is to be ensured that deregistration is not carried out after overlay has been setup.
After the Registry executes the "start <ROUNDS>" command, it first waits till it receives an acknowledgement from all nodes, saying that each node carried out its respective task.
It then waits for 30 secs to give each MessagingNode time to update its counters and then sends a request for summary on each MessagingNode.
So, in order to get the full summary at the Registry side, it takes more than 30 seconds. 
